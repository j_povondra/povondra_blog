from django.db import models

import sys
# tags
from taggit.managers import TaggableManager

# Thumbnail
from PIL import Image 
from io import BytesIO 
from django.core.files.uploadedfile import InMemoryUploadedFile

import uuid

class Post(models.Model):
	title = models.CharField(max_length=250)
	content = models.TextField()
	date_published = models.DateField(auto_now_add=True)
	slug = models.SlugField(unique=True,max_length=100,default=uuid.uuid1)
	tags = TaggableManager()
	image = models.ImageField(null=True,blank=True,upload_to="images/",default="images/noimage.jpg")
	thumbnail = models.ImageField(null=True,blank=True,upload_to="thumbnail/",default="thumbnail/nothumbnail.jpg")


	def __str__(self):
		return self.title

	def save(self, **kwargs):
		if self.image.name != "images/noimage.jpg":
			output_size = (1280, 1024)
			output_thumb = BytesIO()
			img = Image.open(self.image)
			img_name = self.image.name.split('.')[0]
			img.thumbnail(output_size)
			img.save(output_thumb,format='JPEG')

			self.thumbnail = InMemoryUploadedFile(output_thumb, 'ImageField', f"{img_name}_thumb.jpg", 'image/jpeg', sys.getsizeof(output_thumb), None)

		super(Post, self).save()	