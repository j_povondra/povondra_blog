from django.test import TestCase
from Posts.models import Post


class TestURL(TestCase):

	def test_homepage(self):
		response = self.client.get("/")
		self.assertEqual(response.status_code,200)

	def test_tagpage(self):
		testpost = Post.objects.create(title="Django testing",content="Content..",slug="django-testing")
		testpost.tags.add("test","green")
		response = self.client.get(f"/tag/green")
		self.assertEqual(response.status_code,200)	

	def test_detailpage(self):
		testpost = Post.objects.create(title="Django testing",content="Content..",slug="django-testing")
		response = self.client.get(f"/posts/django-testing")
		self.assertEqual(response.status_code,200)

class TestPostModel(TestCase):

	def test_model_str(self):
		testpost = Post.objects.create(title="Django testing",content="Content..",slug="django-testing")
		self.assertEqual(str(testpost),"Django testing")

	def test_thumbnail(self):
		testpost = Post.objects.create(title="Django testing",content="Content..",slug="django-testing")
		self.assertNotEqual(testpost.thumbnail,None)
		self.assertNotEqual(testpost.thumbnail,testpost.image)